﻿using SpamKlasifikavimas.EFCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpamKlasifikavimas
{
    class Tester
    {
        WordType actual;

        public double Slenkstis = 0.7;
        public double DefaultPSW = 0.7;
        public double DeviationPoint = 0.7;
        public double NoSpamInfo = 0.7;
        public double NoHamInfo = 0.7;


        public int leksemos = 10;

        IEnumerable<string> DataLocation;
        public Tester(IEnumerable<string> _DataLocation, double slenkstis = 0.7, double defaultPSW = 0.4, double deviationPoint = 0.5, double noSpamInfo = 0.1, double noHamInfo = 0.99)
        {
            DataLocation = _DataLocation;
            Slenkstis = slenkstis;
            DefaultPSW = defaultPSW;
            DeviationPoint = deviationPoint;
            NoSpamInfo = noSpamInfo;
            NoHamInfo = noHamInfo;
        }
        public List<TestResult> Test(WordType actual)
        {
            this.actual = actual;
            if (DataLocation.Count() > 0)
            {
               return  ReadFiles(DataLocation);
            }
            return new List<TestResult>();
        }

        private List<TestResult> ReadFiles(IEnumerable<string> locations)
        {
            var leksemos = new List<Leksema>();
            var result = new List<TestResult>();
            foreach (var location in locations)
                result.Add(Start(location, -1, leksemos, out leksemos));

            return result;
        }


        private TestResult Start(string path, int thread, List<Leksema> leksemos, out List<Leksema> newleksemos)
        {
            string[] lines = File.ReadAllLines(path);
            var result = Clasify(lines, leksemos);
            if(thread > -1)
              Console.WriteLine($"Reading {path.Split('\\').Last()} at  thread: [{thread}], result:  {Math.Round(result.probability, 5) * 100}%  {result.type.ToString()} | actual: {actual.ToString()}");
            else
                Console.WriteLine($"Reading {path.Split('\\').Last()}, result: {Math.Round(result.probability, 5) * 100}%  {result.type.ToString()} | actual: {actual.ToString()} ");
            newleksemos = result.leksemos;
            return new TestResult()
            {
                text = String.Join(' ', lines),
                probability = result.probability,
                result = result.type,
                real = actual
            };
        }

        private Clarification Clasify(string[] lines, List<Leksema> leksemos)
        {
            var currLeksemos = new List<Leksema>();
            foreach (var line in lines)
                foreach (var word in line.ToLower().Split(Trainer.delimeters))
                {
                    if (!leksemos.Any(x => x.Word.Equals(word, StringComparison.InvariantCultureIgnoreCase)))
                        leksemos.Add(new Leksema(word, Slenkstis, DefaultPSW, DeviationPoint, NoSpamInfo, NoHamInfo));

                   //tikriname tik zodzius ilgesnius nei 2 simboliai
                   if(word.Length > 2)
                        currLeksemos.Add(leksemos.FirstOrDefault(x => x.Word.Equals(word, StringComparison.InvariantCultureIgnoreCase)));
                }
            currLeksemos = currLeksemos.OrderByDescending(x => x.deviation).Take(15).ToList();

            var deviations = currLeksemos.Select(x => x.deviation);

            double psw = 1;
            double minusPws = 1;
            currLeksemos.ForEach(x => psw = psw * x.psw);
            currLeksemos.ForEach(x => minusPws = minusPws * (1 - x.psw));

            var probability = psw / (psw + minusPws);
            return new Clarification()
            {
                probability = probability,
                type = Math.Round(probability, 5) > Slenkstis ? WordType.Spam : WordType.Ham,
                leksemos = leksemos
            };
        }

    }
    class Clarification
    {
        public WordType type { get; set; }
        public double probability { get; set; }
        public List<Leksema> leksemos { get; set; }
    }
    class TestResult
    {
        public string text { get; set; }
        public double probability { get; set; }
        public WordType result { get; set; }
        public WordType real { get; set; }
    }

}
