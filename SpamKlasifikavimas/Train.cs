﻿using SpamKlasifikavimas.EFCore.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpamKlasifikavimas
{
    class Trainer
    {
        IEnumerable<string> SpamDataLocation;
        IEnumerable<string> HamDataLocation;

        public static char[] delimeters = { ' ', '>', '\\', '<', '/' , '!', '?', '.', '-', '=', '+', '@', '(', ')', ',', '&', '%', '^', '#', '`', '~', '|', ':', ';'};
        public Trainer(IEnumerable<string> _SpamDataLocation, IEnumerable<string> _HamDataLocation)
        {
            SpamDataLocation = _SpamDataLocation;
            HamDataLocation = _HamDataLocation;
        }

        public void Train()
        {
            Console.WriteLine("Starting training...");
            if (SpamDataLocation.Count() > 0)
            {
                Console.WriteLine("Training spam");
                ReadFiles(SpamDataLocation, WordType.Spam);
            }

            if (HamDataLocation.Count() > 0)
            {
                Console.WriteLine("Training ham");
                ReadFiles(HamDataLocation, WordType.Ham);
            }
            Console.WriteLine("Training finished");
        }

        private void ReadFiles(IEnumerable<string> locations, WordType type)
        {
            var lists = locations.ToList();
            for (int i = 0; i < locations.Count() - 4; i = i + 4)
            {
                Task[] tasks = new Task[4];

                tasks[0] = Task.Factory.StartNew(() => StartFilter(locations.ElementAt(i), type, i));
                tasks[1] = Task.Factory.StartNew(() => StartFilter(locations.ElementAt(i + 1), type, i + 1));
                tasks[2] = Task.Factory.StartNew(() => StartFilter(locations.ElementAt(i + 2), type, i + 2));
                tasks[3] = Task.Factory.StartNew(() => StartFilter(locations.ElementAt(i + 3), type, i + 3));

                //Wait for the work to complete
                Task.WaitAll(tasks.ToArray());
            }
        }


        private void StartFilter(string path, WordType type, int thread)
        {
            Console.WriteLine($"{path.Split('\\').Last()} at {thread}");
            string[] lines = File.ReadAllLines(path);
            WordsFiltering(lines, type);
        }

        private void WordsFiltering(string[] lines, WordType type)
        {
            var wordsAdded = 0;
            var wordsUpdated = 0;
            var all = 0;
            foreach (string line in lines)
            {
                // zodziai atskirti tarpais, tai skiriam ir cia tarpais
                foreach (var word in line.Split(delimeters))
                {
                    all++;
                    using (var db = new DBContext())
                    {
                        var exsistingWord = db.Words.FirstOrDefault(x => x.Word.Equals(word, StringComparison.InvariantCultureIgnoreCase) && x.Category == type);
                        if (exsistingWord == null)
                        {
                            exsistingWord = new WordFrequency()
                            {
                                Category = type,
                                Count = 1,
                                Word = word.ToLower()
                            };
                            db.Words.Add(exsistingWord);
                            wordsAdded++;
                        }
                        else
                        {
                            wordsUpdated++;
                            exsistingWord.Count++;
                        }

                        db.SaveChanges();
                    }
                }
                //  Console.WriteLine($"Total {type.ToString()} words: {db.Words.Where(x => x.Category == type).Count()}");
            }
            Console.WriteLine($"Added: {wordsAdded} | Updated: {wordsUpdated} | All: {all}");

        }

    }
}
