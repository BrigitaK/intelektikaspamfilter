﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpamKlasifikavimas.EFCore.Models
{
    class DBContext : DbContext
    {

       // private const string connectionString = @"Server=LENOVO-PC\SQLEXPRESS;Database=SpamKlasifikavimas;Trusted_Connection=True;";
  		// private const string connectionString = @"Server=NAMAI\SQLEXPRESS;Database=SpamFilterKryzmine;Trusted_Connection=True;";
  		private const string connectionString = @"Server=NAMAI\SQLEXPRESS;Database=SpamFilter;Trusted_Connection=True;";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<WordFrequency> Words { get; set; }

    }

}
