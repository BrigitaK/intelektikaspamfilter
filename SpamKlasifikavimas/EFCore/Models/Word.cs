﻿using System;

namespace SpamKlasifikavimas.EFCore.Models
{
    public class WordFrequency
    {
        public int Id { get; set; }
        public string Word { get; set; }
        public int Count { get; set; }
        public WordType Category { get; set; }
    }
}