﻿using SpamKlasifikavimas.EFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpamKlasifikavimas
{ 
    class Leksema
    {
        public string Word { get; set; }

        public double psw = 0.7;
        public double deviation = 0;
        public double deviationPoint = 0.7;
        public double NoSpamInfo = 0.7;
        public double NoHamInfo = 0.7;


        public Leksema(string word, double slenkstis = 0.7, double defaultPSW = 0.4, double _deviationPoint = 0.5, double noSpamInfo = 0.1, double noHamInfo = 0.99)
        {
            this.Word = word.ToLower();
            psw = defaultPSW;
            deviationPoint = _deviationPoint;
            NoSpamInfo = noSpamInfo;
            NoHamInfo = noHamInfo;
            Count();
        }

        private void Count()
        {
            using (var db = new DBContext())
            {
               var spam = db.Words.FirstOrDefault(x => x.Word.Equals(Word, StringComparison.InvariantCultureIgnoreCase) && x.Category == WordType.Spam);
               var ham = db.Words.FirstOrDefault(x => x.Word.Equals(Word, StringComparison.InvariantCultureIgnoreCase) && x.Category == WordType.Ham);

                var spamTotal = db.Words.Where(x => x.Category == WordType.Spam).Count();
                var hamTotal = db.Words.Where(x => x.Category == WordType.Ham).Count();

                double pws = 0;
                double pwh = 0;

                if (spam != null)
                    pws = (double)spam.Count / (double)spamTotal;

                if (ham != null)
                    pwh = (double)ham.Count / (double)hamTotal;

                // naujas zodis
                if (spam != null & ham != null)
                    psw = (double)pws / (double)(pwh + pws);
                else if (spam == null)
                    psw = NoSpamInfo;
                else if (ham == null)
                    psw = NoHamInfo;

                if (psw < deviationPoint)
                {
                    deviation = deviationPoint - psw;
                }
                else
                {
                    deviation = psw - deviationPoint;
                }
            }
        }


    }
}
