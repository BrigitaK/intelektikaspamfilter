﻿using SpamKlasifikavimas.EFCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpamKlasifikavimas
{
    class Program
    {
       //static string path = @"C:\Users\Brigita\Desktop\KTUYear3\Pavasaris\Intelektika\L4\intelektikaspamfilter\SpamKlasifikavimas\Assets\";

 		static string path = @"C:\Users\Brigita\Desktop\L4\intelektikaspamfilter\SpamKlasifikavimas\Assets\";
        static void Main(string[] args)
        {

            Console.WriteLine("Do you want  to re-train the network?  Y | N");
            if (Console.ReadLine().Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                Train();

            Console.WriteLine("Starting to test");
            Test();
            Console.WriteLine("Program finished");
            Console.ReadKey();
            Environment.Exit(0);
        }

        private static void Test()
        {
            Task[] tasks = new Task[2];
            var spamResult = new List<TestResult>();
            var spamTester = new Tester(Directory.EnumerateFiles(path + @"\spam_all_test2", "*.txt"), 0.9);
            tasks[0] = Task.Factory.StartNew(() => spamResult = spamTester.Test(WordType.Spam));


            var hamResult = new List<TestResult>();
            var hamTester = new Tester(Directory.EnumerateFiles(path + @"\ham_all_test1", "*.txt"), 0.9);
            tasks[1] = Task.Factory.StartNew(() => hamResult = hamTester.Test(WordType.Ham));

            //Wait for the work to complete
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"Spam kaip spam: {spamResult.Where( x => x.result == WordType.Spam).Count()} ");
            Console.WriteLine($"Spam kaip ham: {spamResult.Where( x => x.result == WordType.Ham).Count()} ");
            Console.WriteLine($"Ham kaip ham: {hamResult.Where( x => x.result == WordType.Ham).Count()} ");
            Console.WriteLine($"Ham kaip spam: {hamResult.Where( x => x.result == WordType.Spam).Count()} ");

        }

        private static void Train()
        {
             var trainer = new Trainer(Directory.EnumerateFiles( path + @"\spam_kryzmine10", "*.txt"), Directory.EnumerateFiles( path + @"\ham_kryzmine10", "*.txt"));
            // var trainer = new Trainer(new List<string>(), Directory.EnumerateFiles( path + @"ham", "*.txt"));
             trainer.Train();
        }

    }
}
